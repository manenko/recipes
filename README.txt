= README

== What this is all about

This book is a collection of Ukrainian Cuisine recipes.

You can find the latest version of the book on the following page:
https://bitbucket.org/manenko/recipes/downloads

== How to build the book manually

These are OS X instructions, but you could use them for GNU/Linux too. The idea
remains the same, just use your package manager to install things. Paths to
files could be different too.

First install `docbook` and `asciidoc` using Homebrew:

....
brew install asciidoc docbook
....

Then edit the file `/usr/local/Cellar/asciidoc/8.6.9/bin/a2x.py` (you might need
to add write permissions to the file, if so, remove them once you finish editing
file) in the following way:

Add `import codecs` to the file and then replace `write_file` function with:

[source,python]
----
def write_file(filename, data, mode='w', encoding='utf-8'):
    f = codecs.open(filename, mode, encoding)
    try:
        f.write(data)
    finally:
        f.close()
----

This is needed to make `a2x` work more or less correctly with non-English
languages. In my case it is Ukrainian.

Then (I don't remember if this is needed or not. Try without these commands
first):

....
sudo mkdir /etc/xml
sudo ln -s /usr/local/etc/xml/catalog /etc/xml/catalog
....

Now just `cd` to book's root folder and run `epub.sh` script from there. It
should build `epub` version of the book.
